# Machine Mayhem '19

Wrangling a machine together can be arduous. You can do anything you'd like, but you only have a week, and are maybe not experts yet. To that end, fab class provides - in the shape of this brief page, and its children - a small set of advice, a suggested direction, and a kit of parts and controllers that should get you to *most* of where you might like to go.

[**mechanical design: PGD**](https://gitlab.cba.mit.edu/jakeread/pgd)  
[**control architecture: squidworks**](https://gitlab.cba.mit.edu/squidworks/squidworks)  
[**browser dataflow: cuttlefish**](https://gitlab.cba.mit.edu/squidworks/cuttlefish) (must be logged in to gitlab)  
[**embedded dataflow: ponyo**](https://gitlab.cba.mit.edu/squidworks/ponyo) (must be logged in to gitlab)  
[**router circuit**](https://gitlab.cba.mit.edu/squidworks/routerboard-atsamd51) (must be logged in to gitlab)  
[**stepper circuit**](https://gitlab.cba.mit.edu/squidworks/moduledaughter-stepper) (must be logged in to gitlab)  
[**module circuit**](https://gitlab.cba.mit.edu/squidworks/moduleboard-atsamd51) (must be logged in to gitlab)  
[**power routing**](https://gitlab.cba.mit.edu/squidworks/powerdistribution)

### Recitation

setup:
x: port a
z: port b ?
ee: port d
yl: port e (-80spu)
yr: port f

... install adafruit samd boards from arduino library manager

... serialport no callback?

## Hardware Kit

You have enough material and parts to make up to four of the: [**Platonic Gantry Designs**](https://gitlab.cba.mit.edu/jakeread/pgd) documented at [that link](https://gitlab.cba.mit.edu/jakeread/pgd). These look like this:

![pgd](images/2019-11-18_pgd-1.jpg)

![pgd-parts](images/2019-11-19_pgd-dwg.png)

Further documentation for these things is at the page [here](https://gitlab.cba.mit.edu/jakeread/pgd): they are parametric - can be fabricated at a handful of lengths and widths - and some collection of them can be cobbled together (with well-designed mechanical appendages, or duct tape - all valid answers) to make multi-degree-of-freedom machines.

The collection of mechanical bits you have access to is as follows:

Thing | Size | QTY | What For? | Vendor | PN / Link
--- | --- | --- | --- | --- | ---
Acrylic | 1/4" 12x24" | 4 | Chassis / Beams | McMaster | 8505K755
625ZZ Bearings | 5x16x5mm | 45 | Rollers | VXB | [link](https://www.vxb.com/20-625ZZ-Shielded-5mm-Bore-Diameter-Miniature-p/625zz20.htm) or McMaster 6153K113
Shoulder Bolts | 5x6xM4 | 45 | Bearing Shaft | McMaster | 92981A146
Bearing Shims | 5x10x0.5mm | 64 | Bearing Standoffs | McMaster | 98089A331
Heat-Set M4 Tapered Inserts | M4 | 50 | Shoulder Bolts -> 3DP Parts | McMaster | 94180A351
Heat-Set M3 Tapered Insert | M3 | 64 | Mechanical Design w/ Plastic | McMaster | 94180A331
Nylon Locknuts | M3 | 200 | McMaster | 93625A100
M3 SHCS | 16mm Long | Beam Joinery | 200 | McMaster | 91292A115
M3 SHCS | 10mm Long | Motor Mounting | 16 | McMaster | 91292A113
M3 FHCS | 16mm Long | Carriage Joinery | 100 | McMaster | 92125A134
M3 Washers | - | w/ all M3 SHCS | 200 | McMaster | 93475A210
20T or 16T GT2 Pulley | 10mm or Wider, 5mm Bore | 4 | Power Transmission | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/BALITENSEN-Timing-Synchronous-Makerbot-Printer/dp/B07G81644C/)
6mm Wide GT2 Belt | - | 5m | Power Transmission | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/Black-Timing-Printer-Machine-Meters/dp/B07PWL37RQ/)

## Circuit Kit

To coordinate machine control, you have a set of controllers developed as part of the [squidworks project](https://gitlab.cba.mit.edu/squidworks/squidworks). [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) is a protocol / tool / scheme for distributed control of modular hardwares. It nests virtual dataflow interpreters inside of genuine dataflow networks, to organize modular code inside of modular hardware. It's graphs all the way down.

The circuits you have here are already bootloader'd and code-loaded: routers will boot to USB, and should be accessible to cuttlefish once you're running it - and steppers have a stepper-motor build already running onboard: you should be set to connect them, power them up, and set up a control network.

| **The Router (1)** |
| --- |
| A message passing device, this thing hooks 6 of the ATSAMD51's SERCOM USARTS up to RS-485 Differential Driver and then hooks those up to RJ10 connectors (read: phone handset jacks). It runs [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) and you can see the [schematic, board and documentation here](https://gitlab.cba.mit.edu/squidworks/routerboard-atsamd51). |
| ![router](images/2019-11-17_router.jpg) |

| **The Module Board** |
| --- |
| A do-what-you-will-with-it device, this thing breaks all of the ATSAMD51's pins out to fab-lab-friendly sized castellated pins, so that you can solder it to some-circuit-of-your-design. The thing is ready to run [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo), and invites you to, indeed, write some CPP and integrate some new devices onto the network that it will happily join over that RS-485 link. [Schematic, board and documentation here](https://gitlab.cba.mit.edu/squidworks/moduleboard-atsamd51). |
| ![module](images/2019-10-30_mw-module.jpg) |

| **The Steppers (4)** |
| --- |
| A motor-turning device, this thing is one of the aforementioned module-boards, soldered to a heavy duty, motor-wrastling, no-amps-barred TMC262 stepper driver which *itself* slices and dices 24v of *power* with the help of four (4!) PN-Pair mosfets (that's two whole h-bridges baby) to drive (probably) NEMA17 stepper motors, to which these things will be attached when you get them. This also runs [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) and you can see the [schematic, board and documentation here](https://gitlab.cba.mit.edu/squidworks/moduledaughter-stepper). |
| ![stepper](images/2019-11-18_stepper.jpg) |

| **Power Distribution Bits** |
| --- |
| These are tiny bus-bar type devices, that should make power routing a little bit easier. Included in the kit are (1) bypass capacitors for spare charge (these are actually important for the stepper motors to work properly), (2) TVS Diodes and Bleeders, (3) 5V Regulators (also necessary) and (4) routing blocks. These are all documented and explained in [this power distribution repo](https://gitlab.cba.mit.edu/squidworks/powerdistribution). |
| ![psu](images/2019-10-30_mw-psu.jpg) |

## Electrical Kit

Circuits need power, and networks need wires. To hook it all up, you also have this list of parts in the kit:

Thing | QTY | What For? | Vendor | PN / Link
--- | --- | --- | --- | ---
PSU-24-350 | 1 | 350W of *watts* | MeanWell | [Amazon](https://www.amazon.com/LRS-350-24-Switching-Supply-350-4W-115Vac/dp/B07RF38JXK)
NEMA17 | 4 | Spinning | [Amazon](https://www.amazon.com/Zyltech-Stepper-Motor-Connector-Printer/dp/B01GNAGJI2/) [Amazon](https://www.amazon.com/Zyltech-Stepper-Motor-Connector-Printer/dp/B01GNJ1XNY/)
Modular Data Cable RJ9 | 100ft | Channel for RS485 UART | Digikey | AT-K-26-4-B/100 |
Modular Data Plugs RJ9 | ~ 25 | RS485 Plugs | Digikey | AE10314-ND‎ |
Modular Data Line Tool RJ9 | 1 | Making RJ10 Cables | Amazon / Commodity | [Amazon](https://www.amazon.com/Knoweasy-Network-Crimper-Telephone-Crimping/dp/B07LDMRSNY/) |
Orange Hook-Up Wire | 50ft | Routing 24V | Digikey | CN101A-50-ND‎ |
Red Hook-Up Wire | 50ft | Routing 5V | Digikey | CN101R-50-ND‎ |
Black Hook-Up Wire | 50ft | Routing Ground | Digikey | CN101B-50-ND‎ |
Power Entry Module | 1 | AC Hookup, Switching | Digikey | 486-3979-ND‎ |
Power Entry Fuse | 1 | *Danger* Barrier (already installed in the above) | Digikey | 486-1226-ND‎ |

### Network Cables

Some notes on making network cables:

- network cables can be made two ways: only one is correct - *rj10 tabs should be on the same side of the ribbon cable* i.e. the cable is a 'straight through' type, not a crossover. this means that tx meets rx, etc.
- the transmit / receive ports are RS-485 Differential Driven, meaning there is no common gnd connection between boards besides the power bus.

The connectors I use here (and that are in your kit) are called 'RJ10' Jacks and Plugs. These are standard for old (wired!) phone handsets, but also 'generally useful'.

One cool thing about RJ45 is the modularity of the cables. We can use commodity crimping tools to make our own lengths:
- one side cuts, one side strips. use both at the same time to get the right length of stripped wire
- use the '4p' crimp, note the tab direction in the crimp
- pinch! the plug has a plastic tab inside that should come down to meet the wire jacket

`these videos show an RJ45 connector: the RJ10 is nearly identical, just smaller`

![rj45 video](images/rj45-assembly.mp4)

`make sure those tabs are on the same side of the flat cable`

![rj45](images/rj45-tabs.jpg)

## Developing Controllers

### Squidworks

Since you have the circuits, you are free to implement a [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) controller using [cuttlefish (browser code)](https://gitlab.cba.mit.edu/squidworks/cuttlefish) and [ponyo (embedded code)](https://gitlab.cba.mit.edu/squidworks/) For more documentation on squidworks, follow the link.

![squids](images/2019-07-21-l1.png)

!TODO: video from webcam-to-penplotter
